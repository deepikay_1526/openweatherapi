# REST API Assessment #
This project is a REST API test using Postman for Openweathermap api.
Test case has been exported and stored as a postman collection.

## Installation ##
#### To run test in postman UI ####
To run this test Install [Postman](https://www.postman.com/downloads/), Export the collection file inside the postman and fire the API.
Test results and logs can be viewed in the postman console and test results tab respectively. 
#### To run test in command line ####
To run this test in command line 
- Install [Node.js](https://nodejs.org/en/download/)
- Install Newman using following npm command
```console
$ npm install -g newman
```
Download the collection to your local and navigate to the downloaded path.
Run the collection file with below command
```console
$ newman run weather-api.postman_collection.json
```
Test results and logs can be viewed in the same console

![Result Console](img/newman-console.PNG)
